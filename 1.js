/////////////////////////
// Explain why `{ a: 1 } === { a: 1 }` is `false` in JavaScript.
///////////////////////

// Answer: When comparing two objects you are comparing the reference not the value.

const obj1 = { a: 1 };
const obj2 = { a: 1 };

console.log(obj1 === obj2);
// false

console.log(obj1 == obj2);
// false

const obj3 = obj1;
console.log(obj1 == obj3); // as the reference is same
// true
