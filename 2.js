/////////////////////////
// Describe the runtime difference(s) between `for await (const a
// of [p1, p2, p3]) { ... }` vs `[p1, p2, p3].forEach(async (p) { await p })`.
///////////////////////

// Answer: forEach will execute the block but not wait for it to complete

const promises = [3, 2, 1].map((a) => ({
  waitTime: a * 100,
  p: new Promise((resolve) => setTimeout(resolve, a * 100)),
}));

promises.forEach(async ({ waitTime, p }) => {
  await p;
  console.log({ waitTime });
});

// Output:
// { waitTime: 100 }
// { waitTime: 200 }
// { waitTime: 300 }

async function f() {
  for await (const { waitTime, p } of promises) {
    await p;
    console.log({ waitTime });
  }
}
f();
// Output:
// { waitTime: 300 }
// { waitTime: 200 }
// { waitTime: 100 }
