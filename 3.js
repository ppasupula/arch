/////////////////////////
// Describe good use cases for using a JavaScript `Map` instead of an `object`.
///////////////////////

// Answer:
// In general I prefer to use Map for key value pairs. It is a better data structure to use when adding/changing key,values at runtime.
// also, Map until functions like .keys , .size makes it handy to use and are easy to iterate
