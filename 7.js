///////////////////////
// Write a function that will employ a divide-and-conquer algorithm that will sort an array of objects
// by their `dateHired` (native JS `Date` object) property in descending order.
// All items in the array will be an object which has a `dateHired` property which is a valid
///////////////////////

const mergeByKey = (key, leftArr = [], rightArr = [], order) => {
  const result = [];
  let leftArrindex = 0;
  let rightArrIndex = 0;

  const comparator = (a, b) => (order === "DESC" ? a > b : a < b);

  while (leftArrindex < leftArr.length && rightArrIndex < rightArr.length) {
    if (comparator(leftArr[leftArrindex][key], rightArr[rightArrIndex][key])) {
      result.push(leftArr[leftArrindex]);
      leftArrindex++;
    } else {
      result.push(rightArr[rightArrIndex]);
      rightArrIndex++;
    }
  }

  return [
    ...result,
    ...leftArr.slice(leftArrindex),
    ...rightArr.slice(rightArrIndex),
  ];
};

const sortByDateHired = (candidates, order = "DESC") => {
  if (!candidates || candidates.length <= 1) return candidates;

  const halfIndex = Math.trunc(candidates.length / 2);

  return mergeByKey(
    "dateHired",
    sortByDateHired(candidates.slice(0, halfIndex)),
    sortByDateHired(candidates.slice(halfIndex)),
    order
  );
};

// Example
console.log(
  sortByDateHired([
    {
      name: "bob",
      dateHired: new Date(),
    },
    {
      name: "alice",
      dateHired: new Date("2022-1-1"),
    },
    {
      name: "chuck",
      dateHired: new Date("2022-2-2"),
    },
    {
      name: "noris",
      dateHired: new Date("2022-3-3"),
    },
  ])
);
