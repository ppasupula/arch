/////////////////////////
// Describe at least one way to substitute a local, unpublished version of a
// JavaScript library in place of a another version that is already published in an NPM registry.
///////////////////////

// Answer: Use Npm link to register a lib in your local npm.
// Step 1: In our unregistered lin run 'npm link'
// Step 2: In your project to use unregistered lib run 'npm link <name of lib>'
// Npm will use the local link for the named matching library. (Roughly speaking it creates a symlink and uses it)
