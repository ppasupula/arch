/////////////////////////
// Write a `jest` mock of method `fetchAllRecords` that will return a `Promise` that
// resolves immediately to array `[1, 2, 3]` that belongs to class `Employee`.
///////////////////////

const Employee = require("./Employee");

describe("Employee class", () => {
  const employee = new Employee();

  const fetchAllRecordsSpy = jest.spyOn(employee, "fetchAllRecords");

  it("should return out the first and last name", () => {
    fetchAllRecordsSpy.mockResolvedValue([1, 2, 3]);

    expect(employee.fetchAllRecords()).resolves.toMatchObject([1, 2, 3]);
  });
});
