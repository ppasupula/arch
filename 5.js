/////////////////////////
// Write a script that will encode the following information sequentially in a JavaScript `Buffer`.
// No need to include field metadata, assume the consumer/decoder is aware of the offsets to use and can parse enum values.
// Make sure to select data types that are optimized for payload size, but can adequately express all possible values given the length and data type:
// `symbol`: utf-8 string up to 4 characters,
// `price`: unsigned 64-bit integer,
// `quantity`: unsigned 64-bit integer,
// `side`: enum buy | sell,
// `type`: enum limit | market
///////////////////////

const symbolSize = 4;
const priceSize = 8;
const quantitySize = 8;
const sideSize = 3;
const typeSize = 5;

function encoder(symbol, price, quantity, side, type) {
  const symbolBuff = Buffer.alloc(symbolSize);
  symbolBuff.write(symbol, 0, 4, "utf-8");

  const priceBuff = Buffer.alloc(priceSize);
  priceBuff.writeBigUInt64LE(price);

  const quantityBuff = Buffer.alloc(quantitySize);
  quantityBuff.writeBigUInt64LE(quantity);

  const sideBuff = Buffer.alloc(sideSize);
  sideBuff.write(side);

  const typeBuff = Buffer.alloc(typeSize);
  typeBuff.write(type);

  return Buffer.concat([
    symbolBuff,
    priceBuff,
    quantityBuff,
    sideBuff,
    typeBuff,
  ]);
}

function encoderOptimized(symbol, price, quantity, side, type) {
  const symbolBuff = Buffer.alloc(
    symbol.length > symbolSize ? symbolSize : symbol.length
  );
  symbolBuff.write(symbol, "utf-8"); // can be a variable size

  const priceBuff = Buffer.alloc(priceSize);
  priceBuff.writeBigUInt64LE(price);

  const quantityBuff = Buffer.alloc(quantitySize);
  quantityBuff.writeBigUInt64LE(quantity);

  const sideBuff = Buffer.alloc(sideSize);
  sideBuff.write(side);

  const typeBuff = Buffer.alloc(typeSize);
  typeBuff.write(type);

  return Buffer.concat([
    symbolBuff,
    priceBuff,
    quantityBuff,
    sideBuff,
    typeBuff,
  ]);
}

module.exports = {
  encoder,
  encoderOptimized,
  symbolSize,
  priceSize,
  quantitySize,
  sideSize,
  typeSize,
};
