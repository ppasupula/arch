/////////////////////////
// Write a script that will decode the `Buffer` you generated from question 5, and return an object with fields as described in the question.
///////////////////////

const { reverse } = require("dns");
const {
  encoder,
  encoderOptimized,
  symbolSize,
  priceSize,
  quantitySize,
  sideSize,
  typeSize,
} = require("./5");

function decoderOptimized(buffer) {
  const bufLength = Buffer.byteLength(buffer);
  return [
    buffer.toString(
      "utf-8",
      0,
      bufLength - typeSize - sideSize - priceSize - quantitySize
    ),
    buffer.readBigUInt64LE(
      bufLength - typeSize - sideSize - priceSize - quantitySize
    ),
    buffer.readBigUInt64LE(bufLength - typeSize - sideSize - priceSize),
    buffer.toString(
      "utf-8",
      bufLength - typeSize - sideSize,
      bufLength - typeSize
    ),
    buffer.toString("utf-8", bufLength - typeSize),
  ];
}

console.log(
  decoderOptimized(
    encoderOptimized(
      "bob",
      0xdecafafecacefaden,
      0xdecafafecacefaden,
      "BUY",
      "LIMIT"
    )
  )
);

// Output:
// [ 'bob', 16053919793946753758n, 16053919793946753758n, 'BUY', 'LIMIT' ]

// Since the vaiable data is at the beginning, if we read data reverse, we don't have to use all the 4 bytes for symbol and can save space
// Observe that bob is read as 'bob' instead of 'bob{garbage}'
