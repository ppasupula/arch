/////////////////////////
// Expain the differences between `object`s and `enum`s in TypeScript.
///////////////////////

// Answer:


// Enums are mainly used to declare named constants

enum MyEnum {
  ONE = 1,
  TEW = 2
}

MyEnum.ONE = 2 // is not possible as they are constants

const MyObj = {
  ONE: 1,
  TWO: 2
}

MyObj.ONE = 100 // will NOT error


// 'as const' can be used on obj that will make MyObj behave as an enum
const MyObj2 = {
  ONE: 1,
  TWO: 2
} as const
MyObj2.ONE = 100 // is not possible
